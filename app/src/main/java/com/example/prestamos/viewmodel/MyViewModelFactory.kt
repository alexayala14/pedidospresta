package com.example.prestamos.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.prestamos.domain.DatosUseCase

class MyViewModelFactory(val datosUseCase: DatosUseCase):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(DatosUseCase::class.java).newInstance(datosUseCase)
    }
}