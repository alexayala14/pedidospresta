package com.example.prestamos.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.prestamos.`interface`.Api
import com.example.prestamos.domain.DatosUseCase
import com.example.prestamos.ui.modelo.Datos
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyViewModel(val datosUseCase: DatosUseCase):ViewModel() {

    private val listData = MutableLiveData<List<Datos>>()

    init {
        getListaDatos()
    }

    fun setListData(listaDatos:List<Datos>){
        listData.value = listaDatos


    }

    fun getListaDatos(){
        setListData(datosUseCase.obtenerListaDatos())

    }

    fun getListaDatosLiveData():LiveData<List<Datos>>{

        return listData
    }


}