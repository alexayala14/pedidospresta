package com.example.prestamos.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.prestamos.data.DatosDataSetPro
import com.example.prestamos.ui.modelo.DatosToken

class MyViewModelPro(application:Application):AndroidViewModel(application) {

    private var datosDataSetPro: DatosDataSetPro = DatosDataSetPro.getInstance(this.getApplication())
    fun addUser(){
        datosDataSetPro.addUser()
    }
    fun getUsers() :MutableLiveData<List<DatosToken>>{
        return datosDataSetPro.getUsers()
    }

}