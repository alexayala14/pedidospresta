package com.example.prestamos.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.prestamos.R
import com.example.prestamos.`interface`.VolleySingleton
import com.example.prestamos.ui.MainActivity
import com.example.prestamos.data.DatosDataSet
import com.example.prestamos.base.BaseViewHolder

import com.example.prestamos.ui.modelo.Datos
import com.example.prestamos.ui.modelo.DatosToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.pedidos.view.*
import org.json.JSONObject
lateinit var preferences: SharedPreferences
class recyclerAdapter(private val context:Context,private var arrayList: List<DatosToken>):RecyclerView.Adapter<BaseViewHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return MainViewHolder(LayoutInflater.from(context).inflate(R.layout.pedidos, parent, false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is MainViewHolder -> holder.bin(arrayList[position], position)
        }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class MainViewHolder(itemView: View) : BaseViewHolder<DatosToken>(itemView) {
        override fun bin(item: DatosToken, position: Int) {
            val tit: String = item.firstName + " " + item.lastName
            itemView.titulo.text = tit
            itemView.descripcion.text = item.dni
            if(item.loanStatus=="approve"){
                itemView.imagen.setImageResource(R.drawable.verde)
            }else{
                itemView.imagen.setImageResource(R.drawable.rojo)
            }
            itemView.setOnClickListener {

               withEditTextt(itemView,item.token,item.dni,item.firstName,item.lastName,item.email,item.gender,item.loanStatus)

            }


        }


    }
    fun withEditTextt(view: View,token:String,dni1:String,nombre1:String,apellido1:String,email1:String,genero1:String,loanStatus1:String) {

        val builder = AlertDialog.Builder(view.context)
        //val inflater = LayoutInflaterlater
        builder.setTitle("                      Pedido")
        builder.setTitle("")
        val dialogLayout = LayoutInflater.from(context).inflate(R.layout.alert_dialog_with_edittextt, null)
        val dni  = dialogLayout.findViewById<TextView>(R.id.editTextDni1)
        val nombre  = dialogLayout.findViewById<EditText>(R.id.editTextNombre)
        val apellido  = dialogLayout.findViewById<EditText>(R.id.editTextApellido)
        val email  = dialogLayout.findViewById<EditText>(R.id.editTextEmail)
        val genero  = dialogLayout.findViewById<EditText>(R.id.editTextGenero)
        val estado  = dialogLayout.findViewById<TextView>(R.id.editTextEstado)
        val nombre2  = dialogLayout.findViewById<TextView>(R.id.editTextNombretext)
        val apellido2  = dialogLayout.findViewById<TextView>(R.id.editTextApellidotext)
        val email2  = dialogLayout.findViewById<TextView>(R.id.editTextEmailtext)
        val genero2  = dialogLayout.findViewById<TextView>(R.id.editTextGenerotext)
        dni.setText(dni1)
        nombre.setText(nombre1)
        apellido.setText(apellido1)
        email.setText(email1)
        genero.setText(genero1)
        estado.setText(loanStatus1)

        builder.setView(dialogLayout)
        builder.setPositiveButton("Cerrar") { dialogInterface, i ->

        }
        builder.setNeutralButton("Modificar"){dialogInterface,i ->
            Toast.makeText(context, "Actualizando Datos ", Toast.LENGTH_LONG).show()
            preferences=context.getSharedPreferences("Shared",Context.MODE_PRIVATE)
            val token2 = preferences.getString("token","")
            val nombre2=nombre.text.toString()
            val apellido2=apellido.text.toString()
            val email2=email.text.toString()
            val genero2 = genero.text.toString()

            if(email.text.toString()!=""&&nombre.text.toString()!=""&&genero.text.toString()!=""&&apellido.text.toString()!="") {
            actualizarPedido(token2,token,dni1,email2,nombre2,genero2,apellido2)
            }
            else{
                Toast.makeText(context, "ERROR || Complete todos los campos ", Toast.LENGTH_LONG).show()
            }
        }
        builder.setNegativeButton("Eliminar"){dialogInterface,i ->
            Toast.makeText(context, "Actualizando Datos ", Toast.LENGTH_LONG).show()


            preferences=context.getSharedPreferences("Shared",Context.MODE_PRIVATE)
            val token2 = preferences.getString("token","")

            eliminarPedido(token2,token,dni1)


        }
        builder.show()
    }

    //Funcion que elimina pedido
    fun eliminarPedido(token2:String?,token1:String,dni:String){
       val queue5 = Volley.newRequestQueue(context)

        val jsonObject: JSONObject = JSONObject()
        jsonObject.put("dni",dni)

        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token2}/${token1}.json"
        val url = "https://wired-torus-98413.firebaseio.com/users/${token2}/${token1}.json"

        val stringRequest5 = object : JsonObjectRequest(
            Request.Method.DELETE, url, jsonObject,
            com.android.volley.Response.Listener { response5 ->
                if (response5 != null) {


                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest5)
        val intent = Intent(context, MainActivity::class.java)
        startActivity(context,intent,null)
        (context as Activity).finish()


    }

    //funcion que actualiza el pedido
    fun actualizarPedido(token2:String?,token1: String,dni:String,email:String,firstName:String,gender:String,lastName:String){
        val queue4 = Volley.newRequestQueue(context)
        val jsonObject:JSONObject=JSONObject()
        jsonObject.put("dni",dni)
        jsonObject.put("email",email)
        jsonObject.put("firstName",firstName)
        jsonObject.put("gender",gender)
        jsonObject.put("lastName",lastName)
        //jsonObject.put("loanStatus",estado)


        val url = "https://wired-torus-98413.firebaseio.com/users/${token2}/${token1}.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token2}/${token1}.json"
        val stringRequest4 = object : JsonObjectRequest(Request.Method.PATCH, url, jsonObject,
            com.android.volley.Response.Listener { response4 ->
                if (response4 != null) {


                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest4)

        val intent = Intent(context, MainActivity::class.java)
        startActivity(context,intent,null)
        (context as Activity).finish()
    }

}