package com.example.prestamos.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.GnssNavigationMessage
import android.os.Bundle
import android.os.PersistableBundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.prestamos.R
import com.example.prestamos.`interface`.VolleySingleton
import com.example.prestamos.domain.DatosUseCase
import com.example.prestamos.ui.modelo.Datos
import com.example.prestamos.ui.modelo.DatosToken
import com.example.prestamos.viewmodel.MyViewModel
import com.example.prestamos.viewmodel.MyViewModelFactory
import com.example.prestamos.viewmodel.MyViewModelPro
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.prefs.Preferences


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MyViewModel
    lateinit var viewModelPro: MyViewModelPro
    private var userList = mutableListOf<DatosToken>()
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    //private lateinit var request:JsonObjectRequest
    lateinit var preferences: SharedPreferences

    val token:String="-MOCt0qZh_PTfZ7Irwxg"
    //val token:String=""



    //lateinit var service: ApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //setupRecylerView()
        //setupViewModelAndObserve()
        val button:Button = findViewById(R.id.button)
        viewManager = LinearLayoutManager(this)

        preferences=getSharedPreferences("Shared",Context.MODE_PRIVATE)
        val tok = preferences.getString("token","")
        val editor:SharedPreferences.Editor=preferences.edit()
        editor.putString("token",token)
        editor.apply()
        //getMyViewModelPro()
        obtenerDatosVolley()
        //setupRecylerView()
        button.setOnClickListener(View.OnClickListener {

            withEditText(it)

        })




    }

    fun getMyViewModelPro(){

        viewModelPro = ViewModelProvider(this).get(MyViewModelPro::class.java)

        viewModelPro.getUsers().observe(this, Observer { users ->
            run{
                userList= users as MutableList<DatosToken>
                viewAdapter = recyclerAdapter(this,userList)
                recyclerView = findViewById<RecyclerView>(R.id.recyclerViewVista).apply {
                    setHasFixedSize(true)
                    layoutManager = viewManager
                    adapter = viewAdapter
                }
                recyclerViewVista.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))


                viewAdapter.notifyDataSetChanged()

            }
        })
        viewModelPro.addUser()
    }

    private fun setupRecylerView(){
        recyclerViewVista.layoutManager = LinearLayoutManager(this)
        recyclerViewVista.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))



    }
    //funcion que crea pedido nuevo cargando los datos en un alertdialog
    fun withEditText(view: View) {

        val builder = AlertDialog.Builder(this)
        val inflater = layoutInflater
        builder.setTitle("                Nuevo Pedido")
        val dialogLayout = inflater.inflate(R.layout.alert_dialog_with_edittext, null)
        val dni  = dialogLayout.findViewById<EditText>(R.id.editTextDni)
        val nombre  = dialogLayout.findViewById<EditText>(R.id.editTextNombre)
        val apellido  = dialogLayout.findViewById<EditText>(R.id.editTextApellido)
        val email  = dialogLayout.findViewById<EditText>(R.id.editTextEmail)
        val genero  = dialogLayout.findViewById<EditText>(R.id.editTextGenero)
        builder.setView(dialogLayout)

        builder.setPositiveButton("Crear") { dialogInterface, i ->
            Toast.makeText(applicationContext, "Actualizando Datos ", Toast.LENGTH_LONG).show()
            if(dni.text.toString() != "" && email.text.toString()!=""&&nombre.text.toString()!=""&&genero.text.toString()!=""&&apellido.text.toString()!="") {
                validarEstado(
                    dni.text.toString(),
                    email.text.toString(),
                    nombre.text.toString(),
                    genero.text.toString(),
                    apellido.text.toString()
                )



            }
            else{
                Toast.makeText(applicationContext, "ERROR || Complete todos los campos ", Toast.LENGTH_LONG).show()
            }
            }

        builder.setNegativeButton("Cerrar"){dialogInterface,i ->}
        builder.show()
    }

    fun setupViewModelAndObserve(){
        viewModel = ViewModelProvider(this, MyViewModelFactory(DatosUseCase())).get(MyViewModel::class.java)

        /*val datosObserver = Observer<List<Datos>>{
            for(dato in it) {
                Log.d("Datos:", dato.firstName)
            }

            for((index, value) in it.withIndex()) {
                Log.d("Datos:$index", value.firstName)
            }
        }*/
        val datosObserver = Observer<List<Datos>>{
            for(dato in it) {
                Log.d("Datos:", dato.firstName)


            }
            //recyclerViewVista.adapter = recyclerAdapter(this,it)

            for((index, value) in it.withIndex()) {
                Log.d("Datos:$index", value.firstName)
            }
        }
        viewModel.getListaDatosLiveData().observe(this, datosObserver)
    }


        // funcion que obtiene y carga recyclerview con los datos obtenidos del endpoint
    fun obtenerDatosVolley(){

        val arrayList: ArrayList<DatosToken> = ArrayList<DatosToken>()
        val arrayList3: ArrayList<DatosToken> = ArrayList<DatosToken>()
        //val url = "https://wired-torus-98413.firebaseio.com//.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/-MNjRR1eAX8RygwEhCY6/-MNjWxrEeyJMrDsXxbG6.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}.json"
            val url = "https://wired-torus-98413.firebaseio.com/users/${token}.json"

        val stringRequest = object : JsonObjectRequest(Request.Method.GET, url, null,
            com.android.volley.Response.Listener { response ->
                if (response != null) {

                    val myKeys = response.names()

                    for (i in 0 until myKeys.length()) {

                        val tokenDatos:String ="${myKeys[i]}"

                        obtenerDatos(tokenDatos,arrayList)
                    }
                    /*try {
                        println("ESTA ES LA KEY" + response.names().toString())
                        println("ESTOS SON TODOS:${response.toString()}")
                        println(response.getString("dni"))
                        val dni = response.getString("dni").toString()
                        val email = response.getString("email").toString()
                        val firstName = response.getString("firstName").toString()
                        val lastName = response.getString("lastName").toString()
                        val gender = response.getString("gender").toString()
                        println("Es el dni:${dni}\nEs el email:${email}\nEs el nombre:${firstName}\nEs el apellido:${lastName}\nEs de genero:${gender}\n")
                        //println(response.toString()[i])
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }*/


                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }
        //queue.add(stringRequest)
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest)
       //arrayList3 = onDataReceived(arrayList2)

    }
    fun obtenerDatos(tokenDat:String,arrayList: ArrayList<DatosToken>){

        val url = "https://wired-torus-98413.firebaseio.com/users/${token}/${tokenDat}.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}/${tokenDat}.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}.json"

        val stringRequest1 = object : JsonObjectRequest(Request.Method.GET, url, null,
            com.android.volley.Response.Listener { response1 ->
                if (response1 != null) {



                    try {

                        val dni = response1.getString("dni").toString()
                        val email = response1.getString("email").toString()
                        val firstName = response1.getString("firstName").toString()
                        val lastName = response1.getString("lastName").toString()
                        val gender = response1.getString("gender").toString()
                        val loanStatus = response1.getString("loanStatus").toString()


                        val datos = DatosToken(
                            tokenDat,
                            dni,
                            email,
                            firstName,
                            gender,
                            lastName,
                            loanStatus
                        )


                        arrayList.add(datos)
                        for (i in arrayList) {

                        }

                        val lista: List<DatosToken> = arrayList.toList()




                        viewAdapter = recyclerAdapter(this,lista)
                        recyclerView = findViewById<RecyclerView>(R.id.recyclerViewVista).apply {
                            setHasFixedSize(true)
                            layoutManager = viewManager
                            adapter = viewAdapter
                        }
                        recyclerViewVista.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))


                        viewAdapter.notifyDataSetChanged()



                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }


                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest1)
        return
    }


    //funcion que valida el estado financiero y llama a la funcion crear pedido
    fun validarEstado(dni:String,email:String,firstName: String,gender: String,lastName: String){

        val url="https://api.moni.com.ar/api/v4/scoring/pre-score/${dni}"

        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/-MNjRR1eAX8RygwEhCY6/-MNjWxrEeyJMrDsXxbG6.json"

        val stringRequest2 = object :JsonObjectRequest(
            Method.GET,
            url,
            null,
            Response.Listener { response2 ->
                if (response2 != null) {

                    val estado: String = response2.getString("status")
                    creaPedido(dni,email,firstName,gender,lastName,estado)


                }

            }, Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"

                return params
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
               headers.put("Content-Type","application/json")
                headers.put("credential","ZGpzOTAzaWZuc2Zpb25kZnNubm5u")

                return headers
            }
        }



        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest2)


    }

    fun creaPedido(dni:String,email:String,firstName:String,gender:String,lastName:String,estado: String){
        println("CREANDO PEDIDO ,${dni},${email},${firstName},${gender},${lastName},${estado}")

        val jsonObject:JSONObject=JSONObject()
        jsonObject.put("dni",dni)
        jsonObject.put("email",email)
        jsonObject.put("firstName",firstName)
        jsonObject.put("gender",gender)
        jsonObject.put("lastName",lastName)
        jsonObject.put("loanStatus",estado)

        val url = "https://wired-torus-98413.firebaseio.com/users/${token}.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}.json"
        val stringRequest3 = object : JsonObjectRequest(Request.Method.POST, url, jsonObject,
            com.android.volley.Response.Listener { response3 ->
                if (response3 != null) {
                    val myKeys1 = response3

                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest3)

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()


    }

    //funcion que crea token en caso de hacer un registro nuevo junto con el nombre del usuario
    fun creaToken(){
        val jsonObject:JSONObject=JSONObject()
        jsonObject.put("usuario","anonymous")


        val url = "https://wired-torus-98413.firebaseio.com/users.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users.json"
        val stringRequest6 = object : JsonObjectRequest(Request.Method.POST, url, jsonObject,
            com.android.volley.Response.Listener { response3 ->
                if (response3 != null) {


                    val tokencreado=response3["name"].toString()
                    preferences=getSharedPreferences("Shared",Context.MODE_PRIVATE)
                    //val tok = preferences.getString("token","")
                    val editor:SharedPreferences.Editor=preferences.edit()
                    editor.putString("token",tokencreado)
                    editor.apply()


                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest6)

    }





    /*fun actualizarPedido(token1: String,dni:String,email:String,firstName:String,gender:String,lastName:String){
        val jsonObject:JSONObject=JSONObject()
        jsonObject.put("dni",dni)
        jsonObject.put("email",email)
        jsonObject.put("firstName",firstName)
        jsonObject.put("gender",gender)
        jsonObject.put("lastName",lastName)
        //jsonObject.put("loanStatus",estado)



        val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}/${token1}.json"
        val stringRequest4 = object : JsonObjectRequest(Request.Method.PATCH, url, jsonObject,
            com.android.volley.Response.Listener { response4 ->
                if (response4 != null) {

                    println("ESTAS EN EL ACTUALIZAR")
                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }
        queue4.add(stringRequest4)


    }*/

   /* fun eliminarPedido(token1:String,dni:String){
        queue5 = Volley.newRequestQueue(this)
        val jsonObject:JSONObject=JSONObject()
        jsonObject.put("dni",dni)



        val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}/${token1}.json"
        val stringRequest5 = object : JsonObjectRequest(Request.Method.DELETE, url, jsonObject,
            com.android.volley.Response.Listener { response5 ->
                if (response5 != null) {

                    println("ESTAS EN EL DELETE")
                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }
        queue5.add(stringRequest5)



    }*/



}

