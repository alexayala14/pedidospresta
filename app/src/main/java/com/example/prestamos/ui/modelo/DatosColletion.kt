package com.example.prestamos.ui.modelo

class DatosColletion: ArrayList<DatosColletionItem>()

data class DatosColletionItem(
    val token:String,
    val datos: Datos)

data class Datos(val dni:String="",
                 val email:String="",
                 val firstName:String="",
                 val gender:String="",
                 val lastName:String="",
                 val loanStatus:String="")
data class DatosToken(val token:String="",
                 val dni:String="",
                 val email:String="",
                 val firstName:String="",
                 val gender:String="",
                 val lastName:String="",
                 val loanStatus:String="")
