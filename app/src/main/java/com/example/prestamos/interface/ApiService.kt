package com.example.prestamos.`interface`

import com.example.prestamos.ui.modelo.Datos
import com.example.prestamos.ui.modelo.DatosColletionItem
import com.google.gson.Gson
import com.squareup.moshi.Moshi
import com.squareup.moshi.JsonAdapter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

import retrofit2.http.*

private const val BASE_URL =
    "https://wired-torus-98413.firebaseio.com/"
    //"https://api.letsbuildthatapp.com/"
/*private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()
*/
//val interceptor = HttpLoggingInterceptor()
//interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
//val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
private val retrofit = Retrofit.Builder()
    .addConverterFactory(ScalarsConverterFactory.create())
     //   .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(BASE_URL)
    .build()

interface ApiService {
    @GET("users.json?print=pretty")
    //@GET("youtube/home_feed")
    fun getProperties(): Call<String>

    @GET("users.json")
    fun getProperties1(): Call<List<DatosColletionItem>>

    @GET("users.json/dni")
    fun getPostById(@Path("dni") dni: String): Call<Datos>

    @POST("users.json/dni")
    fun editPostById(@Path("dni") dni: String, @Body post: Datos?): Call<Datos>
}

object Api {
    val retrofitService : ApiService by lazy {
        retrofit.create(ApiService::class.java) }
}