package com.example.prestamos.domain

import com.example.prestamos.data.DatosDataSet
import com.example.prestamos.ui.modelo.Datos

class DatosUseCase {

    private val datosDataSet = DatosDataSet()

    fun obtenerListaDatos():List<Datos>{
    return datosDataSet.createListOfDatos()
    }
}