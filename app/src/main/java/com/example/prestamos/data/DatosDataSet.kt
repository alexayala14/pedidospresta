package com.example.prestamos.data

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import androidx.collection.LruCache
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.example.prestamos.`interface`.Api
import com.example.prestamos.`interface`.VolleySingleton
import com.example.prestamos.ui.MainActivity
import com.example.prestamos.ui.modelo.Datos
import com.example.prestamos.ui.modelo.DatosColletionItem
import com.example.prestamos.ui.modelo.DatosToken
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback




class DatosDataSet () {

    val token:String="-MNs-GqH8Yvg3r6QsBsD"

    companion object{
        @Volatile
        private var INSTANCE:VolleySingleton? = null
        fun getInstance(context:Context) =
            INSTANCE ?: synchronized(this){
                INSTANCE ?: VolleySingleton(context).also { INSTANCE = it }

            }
    }

    fun createListOfDatos():List<Datos>{





        // Access the RequestQueue through your singleton class.
       // MySingleton.getInstance(DatosDataSet.this).addToRequestQueue(jsonObjectRequest)

        //queue = Volley.newRequestQueue()
        //queue1 = Volley.newRequestQueue()
        //var valor =obtenerDatosVolley()
        //println("VALOR------------ ${valor.toString()}")
        val prueba:Datos=prueba()

        return listOf(
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            Datos("30243565", "pepe@gmail.com", "pepe", "masculino", "argento", "approve"),
            Datos("29336558", "joseperez14@gmail.com", "jose", "masculino", "gonzalez", "rejected"),
            prueba



        )



        //return lista
    }

    fun prueba():Datos{
       val datos:Datos = Datos("00000000", "prueba@gmail.com", "ESUNAPRUEBA", "masculino", "pruebaTAMBIEN", "approve")
        return datos
    }


    fun obtenerDatosVolley(){
        //queue = Volley.newRequestQueue(re)
        //queue1 = Volley.newRequestQueue(this)
        val arrayList: ArrayList<DatosToken> = ArrayList<DatosToken>()
        //val url = "https://wired-torus-98413.firebaseio.com//.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/-MNjRR1eAX8RygwEhCY6/-MNjWxrEeyJMrDsXxbG6.json"
        val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}.json"
        val stringRequest = object : JsonObjectRequest(Request.Method.GET, url, null,
            com.android.volley.Response.Listener { response ->
                if (response != null) {

                    val myKeys = response.names()

                    println("ESTOS SON TODOS ACA:${response.toString()}")
                    println("ES ASIIII ${myKeys[1]}")
                    for (i in 0 until myKeys.length()) {
                        println("ES LA MEJOR:${myKeys[i]}")
                        val tokenDatos:String ="${myKeys[i]}"
                        println(response.getString("${myKeys[i]}"))
                        obtenerDatos(tokenDatos,arrayList)
                    }
                    /*try {
                        println("ESTA ES LA KEY" + response.names().toString())
                        println("ESTOS SON TODOS:${response.toString()}")
                        println(response.getString("dni"))
                        val dni = response.getString("dni").toString()
                        val email = response.getString("email").toString()
                        val firstName = response.getString("firstName").toString()
                        val lastName = response.getString("lastName").toString()
                        val gender = response.getString("gender").toString()
                        println("Es el dni:${dni}\nEs el email:${email}\nEs el nombre:${firstName}\nEs el apellido:${lastName}\nEs de genero:${gender}\n")
                        //println(response.toString()[i])
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }*/


                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }

        //VolleySingleton.getInstance(context).addToRequestQueue(stringRequest)


    }
    fun obtenerDatos(tokenDat:String,arrayList: ArrayList<DatosToken>){
        var datos:DatosToken
        val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}/${tokenDat}.json"
        //val url = "https://users-71177-default-rtdb.firebaseio.com/users/${token}.json"
        println("PASO AL INICIO")
        val stringRequest1 = object : JsonObjectRequest(Request.Method.GET, url, null,
            com.android.volley.Response.Listener { response1 ->
                if (response1 != null) {

                    /*val myKeys = response.names()

                   println("ESTOS SON TODOS ACA:${response.toString()}")
                    println("ES ASIIII ${myKeys[1]}")
                    for (i in 0 until myKeys.length()) {
                        println("ES LA MEJOR:${myKeys[i]}")
                        println(response.getString("${myKeys[i]}"))
                    }*/
                    println("ESTA ES obtener" + response1.names())
                    println("ESTOS obtener:${response1.toString()}")
                    try {
                        println("ESTA ES obtener" + response1.names())
                        println("ESTOS obtener:${response1.toString()}")
                        println(response1.getString("dni"))
                        val dni = response1.getString("dni").toString()
                        val email = response1.getString("email").toString()
                        val firstName = response1.getString("firstName").toString()
                        val lastName = response1.getString("lastName").toString()
                        val gender = response1.getString("gender").toString()
                        val loanStatus = response1.getString("loanStatus").toString()
                        println("Es el dni:${dni}\nEs el email:${email}\nEs el nombre:${firstName}\nEs el apellido:${lastName}\nEs de genero:${gender}\n")

                        datos = DatosToken(
                            tokenDat,
                            dni,
                            email,
                            firstName,
                            gender,
                            lastName,
                            loanStatus
                        )

                        println("LOS DATOS DE LA LISTA SON:---------------")
                        arrayList.add(datos)
                        for (i in arrayList) {
                            println("ESTO ES EL ARRAYLIST DE DATOS TOKEN GET:${i}")
                        }
                        println(arrayList.toString())
                        val lista: List<DatosToken> = arrayList.toList()
                        println("AHORA ESTA EN FORMATO LISTA")
                        println(lista.toString())
                        //createListOfDatos(lista)


                        /*list.add(dni)
                        list.add(email)
                        list.add(firstName)
                        list.add(lastName)
                        list.add(gender)


                        arrayList.addAll(list)*/
                        //println(response.toString()[i])
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }


                }

            }, com.android.volley.Response.ErrorListener { error -> error.printStackTrace() }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = "value"
                return params
            }
        }
        println("PASO AL FINAL")
        //queue1.add(stringRequest1)
        //VolleySingleton.instance?.addToRequestQueue(stringRequest1)
        //VolleySingleton.getInstance(context).addToRequestQueue(stringRequest1)
        return
    }


}
